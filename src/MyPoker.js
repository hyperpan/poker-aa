'use strict'
let Player = require('./Actors/Player');
let TexasHoldemGame = require('./Games/TexasHoldemGame');
let Ranks = require('./Cards/Rank');
let Hand = require('./Hands/Hand');

let evaluator= require('./Eval/Eval');

let game = new TexasHoldemGame();

game.seatPlayer(new Player("Hideo") );
game.seatPlayer(new Player("Yoko")  );
game.seatPlayer(new Player("Peter") );
game.seatPlayer(new Player("Ellie") );
game.seatPlayer(new Player("Roy")   );
game.seatPlayer(new Player("Paula") );
game.seatPlayer(new Player("Alice") );
game.seatPlayer(new Player("Nick")  );

game.newRound();

//ALL players go all in for debugging
game.participants.forEach(p => p.allIn() );


//------------POCKET------------
game.pocket();
//game.printDealtHands();
//-------------FLOP-------------
game.flop();
//game.printDealtHands();
//-------------TURN-------------
game.turn();
//game.printDealtHands();
//------------RIVER-------------
game.river();
game.printDealtHands();
//------------EVAL--------------
game.mergeHands();
game.participants.forEach(p => evaluator.eval(p.hand));
game.printDealtHands();


game.electWinners();
game.announceWinners();
//game.