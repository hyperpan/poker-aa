'use strict'
let Player = require('../Actors/Player');
let TexasHoldemGame = require('../Games/TexasHoldemGame');
let Ranks = require('../Cards/Rank');
let Hand = require('../Hands/Hand');

let evaluator= require('../Eval/Eval');
let HandFactory = require('../Hands/HandFactory');

let hands=[];


//hand.print("");
hands.push(new Hand("4d 9c Ac  3d 2d 5h 4c") )    // STRAIGHT ON 5                    EXPECTED: STRAIGHT, 5 HIGH.
hands.push(new Hand("Qh 10s Jd Kd 10c As 9s") )    // STRAIGHT ON A                    EXPECTED: STRAIGHT, A HIGH.
//hands.push(new Hand("Qh Qd Qs Qc Jh 9h As Kh 10s ")) 
//hands.push(new Hand("As 2c Ac 5s 4c 3c 4c 5c"));

/* EDGE CASES */
hands.push(new Hand("{  A♦  Q♦  6♦  5♦  4♦  3♦ } " ) );

//hands.push(new Hand("4s Qd Ks  Qc 4d Qs 4c") )    // 2 THREE_OF_A_KIND.               EXPECTED: FULL_HOUSE.
//hands.push(new Hand("3h Jd Jc  3c Ks 3d 3s") )    // FOUR_OF_A_KIND, extra pair       EXPECTED: FOUR_OF_A_KIND, King high. 
//hands.push(new Hand("Ks 2s 10c 2c Kd As Qs") )    // THREE_PAIR                       EXPECTED: TWO_PAIR, Ace high.


for(let hand of hands){
    evaluator.eval(hand);
    hand.print();
}