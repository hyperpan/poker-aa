'use strict'
let Player = require('../Actors/Player');
let TexasHoldemGame = require('../Games/TexasHoldemGame');
let Ranks = require('../Cards/Rank');
let Hand = require('../Hands/Hand');

let evaluator= require('../Eval/Eval');
let HandFactory = require('../Hands/HandFactory');

let handFactory = new HandFactory();

let hand = handFactory.newRandomHand(7);
//hand = new Hand(" {  4♦  9♦  J♣  4♠  4♣  J♠ 10♥ }	 ") // DEBUG ERRORS

hand.print();
console.log("-----------------")
evaluator.eval(hand);
hand.print();
