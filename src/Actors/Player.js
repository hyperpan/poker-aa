let Hand = require('../Hands/Hand');

module.exports = class Player{
    constructor(name){
        this.name =name;
        this.balance = 10000;
        this.game = null;
        this.hand = new Hand();
    }

    bet(amount){
        if(amount< this.balance)
            game.pots[0].bets += amount;
            balance  -= amount;
    }

    allIn(){
        //let diff = balance - calcLeastBalance();
        this.game.pots[0].addBet(this.balance) ;
        this.balance = 0;
    }

    fold(){
        this.hand= null;
    }


    printHand(){
        console.log(this.name+":\t"+this.hand.toString());
    }
}

