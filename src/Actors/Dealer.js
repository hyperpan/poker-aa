let Deck = require('../Cards/Deck');
let Hand = require('../Hands/Hand');

module.exports = class Dealer{
    
    constructor(){
        this.deck = null;
    }

    /**
     * Deals n cards to each player.
     * As in a real deal, iterates through the number of players, dealing 1 card in each iteration. 
     * Standard procedure in practically all poker variants.
     * 
     * @param {int} ncards Number of cards to deal to each player
     * @param {list} players A list of players to serve.
     */
    dealPlayers(ncards, players) {        
        for(let i=0;i<ncards;i++)
            for (let player of players)
                this.dealCards(1,player.hand) ;
        
    }


    dealCards(ncards, hand){
        if(hand != null)
            for(let i=0;i<ncards;i++)
                hand.push(this.deck.pop());
    }

    discardOne(){
        this.deck.pop();
    }

    /**
     * shuffles the deck by iterating the cards and swapping them with a random card. 
     */
    shuffleDeck (){
        for (let i = this.deck.cards.length - 1; i > 0; i -= 1) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = this.deck.cards[i]
            this.deck.cards[i] = this.deck.cards[j]
            this.deck.cards[j] = temp;
        }

    }

    openNewDeck(){
        this.deck= new Deck();
    }

}

