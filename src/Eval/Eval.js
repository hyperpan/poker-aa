let Suits = require('../Cards/Suit');
let Ranks = require('../Cards/Rank');
let HandRanks = require('../Hands/HandRank');
let Hand = require('../Hands/Hand');

module.exports = Object.freeze({
    
    eval: function(hand){

        /* Eval Group: same rank groups -> Four of a Kind, Full House, Three of a Kind, Two Pair, One Pair, High Card */
        this.evalgroupRanks(hand);
        
        /* Eval Group: Flush, Straight Flush, Royal Flush */
        let flush = this.evalFlush(hand);
        if(flush){
            let straightFlush = this.evalStraight(flush);
            if(straightFlush){
                if(straightFlush.cards[0].rank==Ranks.ACE)
                    hand.rank=HandRanks.ROYAL_FLUSH;
                else
                    hand.rank=HandRanks.STRAIGHT_FLUSH;
                    
                hand.best5= straightFlush;
                return hand.rank;
            }

            if(hand.rank < HandRanks.FULL_HOUSE){
                hand.rank=HandRanks.FLUSH;
                hand.best5=flush;
                
                return hand.rank;
            }
        }
        
        if(hand.rank < HandRanks.FLUSH){
            let str8 = this.evalStraight(hand);
            if (str8){
                hand.rank=HandRanks.STRAIGHT;
                hand.best5=str8;

                return hand.rank;
            }
        }
        
    },
    
    evalStraight: function (hand){
        if (hand.length<5)
            return false;
            
        let str8 = hand.uniqueRanks();
        str8.sortRanksDesc();
        if (str8.cards[0].rank == Ranks.ACE )
            str8.push(str8.cards[0])
        
        let chain = 1;
        let i=0;
        let indexOfHigh = i;
        let remaining = str8.length()-1;
        let isEnough = true;
        
        while(isEnough && chain <5){
            if(chain ==1)
                indexOfHigh = i;
            if(this.evalConsecutive(str8.cards[i],str8.cards[++i]) )
                chain++;
            else
                chain = 1;
            
            remaining --;
            isEnough = (remaining + chain) > 4;
            
        }
        
        if(chain==5){
            str8= str8.slice(indexOfHigh, indexOfHigh+5);
            //console.log("Straight! "+ str8.toString())
            return str8;
        }
    },
    
    evalFlush: function (hand){
        
        for(let suit of Suits){
            let suited = hand.filter( card => card.suit == suit);
            
            //console.log("Suited: "+suited.toString())
            suited.sortRanksDesc();
            
            if(suited.cards.length>4){
                return suited.slice(0,5);
            }
        }
        
        return false;
        
    },

    evalgroupRanks: function (hand){
        //let highCards =[];
        let group = null;
        let highRank  = HandRanks.HIGH_CARD;
        let lowRank   = HandRanks.HIGH_CARD;
        let groupRank = HandRanks.HIGH_CARD; 
        let highCards  = new Hand();
        let grouped = new Hand();
        let ranks = hand.uniqueRanks();
        ranks.sortRanksDesc();

        for (let card of ranks){
            group = hand.filter( card2 => card2.rank == card.rank);
            
            let total = group.length();
            if(total==4)
                groupRank = HandRanks.FOUR_OF_A_KIND;
            else if (total == 3)
                groupRank = HandRanks.THREE_OF_A_KIND;
            else if (total == 2)
                groupRank = HandRanks.ONE_PAIR;
            else
                groupRank = HandRanks.HIGH_CARD;

            if(groupRank > highRank){                   // Found a group that has more members than the highest group so far. 
                lowRank = highRank;                     // The older high rank now moves to low rank.
                highRank = groupRank;                   // and this rank is the high from now on. 
                for (let card of group)
                    grouped.cards.unshift(card);        // Let's place it in the beginning of the grouped cards. 
            }
            else if(groupRank > HandRanks.HIGH_CARD){   // Found an equally large or smaller group of cards, ranking lower than the high group. 
                                                        // Ranks are sorted (descending), so no chance of a higher ranked group later on. 
                lowRank = groupRank;
                for(let card of group)
                    grouped.push(card);                 // let's push the discovered group to the end of grouped cards.
            }
            else                                        // Last possibility: a single card that cannot be grouped. 
                highCards.push(card);                   // We place it in a different pile, to be merged as a last step. 

        }


        if(highRank == HandRanks.FOUR_OF_A_KIND){
            hand.rank = HandRanks.FOUR_OF_A_KIND;
            
            if(lowRank!= HandRanks.HIGH_CARD){      // On few extreme cases, we may get FOUR_OF_A_KIND as high rank, 
                                                    // followed by a group other than HIGH_CARD in the low rank (ONE_PAIR or even THREE_OF_A_KIND). 
                                                    // We need to eliminate the low rank, and select the highest remaining rank as 5th. 
        
                let lowGroup = grouped.cards.splice(4);   // We remove everything after the FOUR_OF_A_KIND,
                for(let card of lowGroup)           
                    highCards.push(card);           // add each removed card of into the highcards pile,
                highCards.sortRanksDesc();          // sort by rank (descending),
                grouped.push(highCards.cards[0]);   // and select the highest as the 5th card of the hand. Messy, right?
            }

        }
        else if(highRank == HandRanks.THREE_OF_A_KIND)
            if (lowRank != HandRanks.HIGH_CARD)          //AT LEAST 2 cards of the same rank on low. (2 THREE_OF_A_KIND happen in holdem, practically a FULL_HOUSE)
                hand.rank = HandRanks.FULL_HOUSE;            
            else
                hand.rank = HandRanks.THREE_OF_A_KIND;
        else if(highRank == HandRanks.ONE_PAIR)
            if (lowRank == HandRanks.ONE_PAIR){
                hand.rank = HandRanks.TWO_PAIR;
                
                if(grouped.cards.length!=4){                // Rarely, we may get actually get the nonexistent THREE_PAIR.                                                         
                                                            // We need to eliminate anything after the highest two pairs, and select the highest remaining rank as 5th.
                    let lowGroup = grouped.cards.splice(4); // We remove everything after the second pair,
                    for(let card of lowGroup)           
                        highCards.push(card);               // add each removed card of into the highcards pile,
                    highCards.sortRanksDesc();              // sort by rank (descending),
                    grouped.push(highCards.cards[0]);       // and select the highest as the 5th card of the hand. Same algorithm as FOUR_OF_A_KIND. 
                }
            }
            else
                hand.rank = HandRanks.ONE_PAIR;
        else
            hand.rank = HandRanks.HIGH_CARD;

        let i =0;
        while(grouped.cards.length < 5){                //merge the grouped cards with the remaining high cards until we have 5 (already sorted by descending rank)
            grouped.push(highCards.cards[i++]);
        }

        grouped.cards.splice(5);  //in the remaining cases where the grouped cards exceed 5 (eg two THREE_OF_A_KIND).

        hand.best5 = grouped;
        return grouped;

    },



    compareHands : function (hand1, hand2){
        let winning = hand1.rank - hand2.rank;

        if(winning!=0)
            return winning;
        
        let i = 0;
        let diff = 0;
        while (i<5 && diff ==0){
            diff = hand1.cards[i].rank - hand2.cards[i].rank;
            i++;
        }
        return diff;
    },
    
    evalConsecutive: function (card1, card2){
        if ( (card1.rank == Ranks.lower(card2.rank) ) || (card2.rank == Ranks.lower(card1.rank) ) )
            return true;
        
        return false;
    }
    
    
});