let Ranks = require('../Cards/Rank');
let Suits = require('../Cards/Suit');
let Card  = require('../Cards/Card');

let Hand  = require('./Hand.js')
let Hands = require('./HandRank.js');
let Dealer= require('../Actors/Dealer');

module.exports = class HandFactory{
    constructor(){
        this.properties=null;
    }

    newRandomHand(cards=5){
        let dealer = new Dealer();
        let hand = new Hand();

        dealer.openNewDeck();
        dealer.shuffleDeck();
        
        dealer.dealCards(cards,hand);

        return hand;

    }




}
