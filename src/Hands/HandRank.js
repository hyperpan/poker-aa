
module.exports = Object.freeze({
    HIGH_CARD:          1,
    ONE_PAIR:           2,
    TWO_PAIR:           3,
    THREE_OF_A_KIND:    4,
    STRAIGHT:           5,
    FLUSH:              6,
    FULL_HOUSE:         7,
    FOUR_OF_A_KIND:     8,
    STRAIGHT_FLUSH:     9,
    ROYAL_FLUSH:       10,

    properties: Object.freeze({
        1: {name: "High card"},
        2: {name: "One pair"},
        3: {name: "Two pair"},
        4: {name: "Three of a kind"},
        5: {name: "Straight"},
        6: {name: "Flush"},
        7: {name: "Full house"},
        8: {name: "Four of a kind"},
        9: {name: "Straight flush"},
       10: {name: "Royal flush"}
    }),

    [Symbol.iterator]() {
      let curr = this.HIGH_CARD;
      let max  = this.ROYAL_FLUSH;
      let iterator = {
        next: ()=>{
          if(curr <= max){
            return {value: curr++, done:false};
          }
          else{
            return {done: true};
          }
        }
      }
      return iterator;
    },
    
    getName: function (rank){
      return this.properties[rank].name;
    }

  })