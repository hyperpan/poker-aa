let Ranks = require('../Cards/Rank');
let Suits = require('../Cards/Suit');
let Card  = require('../Cards/Card');
let Hands = require('./HandRank.js')

module.exports = class Hand{    

    constructor(str=""){
        this.cards=[];
        this.rank=null;
        this.best5 = null;
        str= str.replace('{','').replace('}','');   //  I want to feed a hand.toString() into a constructor for getting a lazy copy. 
        
        let cards = str.split(" ").map(card => card.trim()).filter(card=> ( (card.length ==2 && card[0]!="1") || (card.length ==3 && card.slice(0,2)=="10") )  );

        for(let card of cards){
            
            let suit = card.slice(-1).toLowerCase();
            switch(suit){
                case 's':
                case '♠':
                    suit = Suits.SPADES;
                    break;
                case 'h':
                case '♥':
                    suit = Suits.HEARTS;
                    break;
                case 'c':
                case '♣':
                    suit = Suits.CLUBS;
                    break;
                case 'd':
                case '♦':
                    suit = Suits.DIAMONDS;
                    break;
                default:
                    suit = null;
            }
            
            let rank = card.slice(0,card.length-1);
            
            if(rank!=null && suit!=null){
                this.cards.push(new Card(Ranks.bySymbol(rank),suit));
                
            }
        }
    }
    
    push(card){
        this.cards.push(card);
    }

    toString(){
        let strRank="";
        if(this.rank!=null){
            strRank="\t - "+Hands.getName(this.rank)
        }

        let strBest5="";
        if(this.best5!=null){
            strBest5 = "\t - Best 5: "+this.best5.toString();
        }

        let str= "{ "
        for(let card of this.cards)
            str=str.concat(card.toString()," ");
        return str.slice(0,str.length-1)+" }\t"+strBest5+strRank;
    }

    [Symbol.iterator]() {
        let index = 0;
        let max  = this.cards.length;
        let iterator = {
          next: ()=>{
            if(index < max){
              return {value: this.cards[index++], done:false};
            }
            else{
              return {done: true};
            }
          }
        }
        return iterator;
      }
      
    sortRanks(){
        this.cards.sort( (card1, card2)  => card1.rank - card2.rank) ;
      }
    sortRanksDesc(){
        this.cards.sort( (card1, card2)  => card2.rank - card1.rank) ;
    }
      
    sortSuits(){
        this.cards.sort( (card1, card2)  => card1.suit < card2.suit) ;
    }

    indexOfRank(rank){
        for(let card of this.cards)
            if(card.rank==rank)
                return this.cards.indexOf(card)
    }

    uniqueRanks(){        
        return this.filter( (card, index) => this.indexOfRank(card.rank) == index )
    }
    
    length(){
        return this.cards.length;
    }

    
    filter(func){
        let filteredCards = this.cards.filter(func);
        let hand = new Hand();
        
        filteredCards.forEach(c => hand.push(c));
        
        return hand;
    }
    

    slice(...args){
        let slicedCards = this.cards.slice(...args);
        let hand = new Hand();
        slicedCards.forEach(c => hand.push(c))
        
        return hand;
    }

    swap(card1, card2){
        
    }


    compareTo (hand2){
        let winning = hand2.rank - this.rank;

        if(winning!=0)
            return winning;
        
        let i = 0;
        let diff = 0;
        while (i<5 && diff ==0){
            diff = hand2.best5.cards[i].rank - this.best5.cards[i].rank;
            i++;
        }
        return diff;
    }
    
    print(){
        console.log(this.toString())
    }
    
};
