
module.exports = Object.freeze({

    DEUCE:  2,
    THREE:  3,
    FOUR:   4,
    FIVE:   5,
    SIX:    6,
    SEVEN:  7,
    EIGHT:  8,
    NINE:   9,
    TEN:    10,
    JACK:   11,
    QUEEN:  12,
    KING:   13,
    ACE:    14,

    properties: Object.freeze({
       2: {name: "two",   symbol: " 2"}, 
       3: {name: "three", symbol: " 3"}, 
       4: {name: "four",  symbol: " 4"}, 
       5: {name: "five",  symbol: " 5"}, 
       6: {name: "six",   symbol: " 6"}, 
       7: {name: "seven", symbol: " 7"}, 
       8: {name: "eight", symbol: " 8"}, 
       9: {name: "nine",  symbol: " 9"}, 
      10: {name: "ten",   symbol: "10"}, 
      11: {name: "jack",  symbol: " J"}, 
      12: {name: "queen", symbol: " Q"}, 
      13: {name: "king",  symbol: " K"}, 
      14: {name: "ace",   symbol: " A"}, 
    }),

    [Symbol.iterator]() {
      let curr = this.DEUCE;
      let max  = this.ACE;
      let iterator = {
        next: ()=>{
          if(curr <= max){
            return {value: curr++, done:false};
          }
          else{
            return {done: true};
          }
        }
      }
      return iterator;
    },

    lower: function (rank){
      if(rank>this.DEUCE)
        return --rank;
      else
        return this.ACE;
    },
    
    bySymbol: function (rankStr) {
      return Object.keys(this.properties).filter(key => this.properties[key].symbol.trim()== rankStr.trim() ).toString()
    },
    
    getSymbol: function (rank){
      return this.properties[rank].symbol;
    }
    
    
})