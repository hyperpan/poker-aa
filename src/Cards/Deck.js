let Ranks = require('./Rank');
let Suits = require('./Suit');
let Card  = require('./Card');

module.exports = class Deck{
    
    constructor(){
        this.cards= this.newStandardDeck();
    }

    newStandardDeck(){
        let stdDeck = [];
        for (let suit of Suits)
            for (let rank of Ranks)
                stdDeck.push(new Card (rank,suit));
        return stdDeck;
    }   //it doesn't order it Ace first, but we're going to shuffle anyway...
    
    push(c) {
        this.cards.push(c);
    }

    pop(){
        return this.cards.pop();
    }

    toString(){
        let str= "{ "
        for(let card of this.cards)
            str=str.concat(card.toString()," ");
        return str.slice(0,str.length-1)+" }";
    }

}

