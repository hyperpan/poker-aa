let Ranks = require('./Rank');
let Suits = require('./Suit');

module.exports = class Card{    
    
    constructor(rank, suit) {
        this.rank = rank;
        this.suit = suit;
    }
    
    toString(){
        return  Ranks.getSymbol(this.rank) +
                Suits.properties[this.suit].symbol;
    }
    
    
    
    
};
