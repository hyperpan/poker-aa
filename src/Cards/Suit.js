module.exports = Object.freeze({
    SPADES   : 1,
    HEARTS   : 2,
    CLUBS    : 3,
    DIAMONDS : 4,

    properties: {
      1: {name: "spades",   symbol: "♠"},
      2: {name: "hearts",   symbol: "♥"},
      3: {name: "clubs",    symbol: "♣"},
      4: {name: "diamonds", symbol: "♦"}
    },
    
    [Symbol.iterator]() {
      let curr = this.SPADES;
      let last = this.DIAMONDS;
      let iterator = {
        next: ()=>{
          if(curr <= last){
            return {value: curr++, done:false};
          }
          else{
            return {done: true};
          }
        }


      }
      return iterator;
    },
    values: ()=>{
      return [...this];
    }

})