let Game = require('./Game');
let Hand = require('../Hands/Hand');

module.exports = class TexasHoldemGame extends Game{
    constructor(){
        super();
        this.community = new Hand();

    }

    newRound(){
        super.newRound();
        this.community=new Hand();
    }

    pocket(){
        this.dealPlayers(2);
    }

    flop(){
        this.dealer.discardOne();
        this.dealer.dealCards(3,this.community);        
    }

    turn(){
        this.dealer.discardOne();
        this.dealer.dealCards(1,this.community);
    }

    river(){
        this.dealer.discardOne();
        this.dealer.dealCards(1,this.community);
    }
    
    /**
     * merges the community cards into each player's hand for the final hand evaluation. 
     */
    mergeHands(){
        for(let player of this.participants)
        for(let card of this.community.cards)
            player.hand.push(card);
    }


    printCommunity(){
            if(this.community.cards.length>0)
                console.log("Table:\t"+this.community.toString() );
    }

    printDealtHands(){
        
        this.printCommunity();
        this.printPlayerHands();
        console.log("-------------------");
    }

}


