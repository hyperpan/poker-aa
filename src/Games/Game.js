let Dealer = require('../Actors/Dealer');
let Hand = require('../Hands/Hand');
let Pot = require('./Pot');

module.exports = class Game{
    constructor(){
        this.dealer = new Dealer();
        this.seats=9;
        this.players=[];
        this.participants=[];
        this.buttonPosition= 0;
        this.pots=[];
        this.ante=0;
        this.big_blind= 200;
        this.small_blind=100;
    }


    newRound(){
        this.participants= this.players.filter( player => player.balance >= this.big_blind) ;
        this.pots = [];
        this.pots.push(new Pot() );


        this.dealer.openNewDeck();
        this.dealer.shuffleDeck();
        for(let player of this.participants){
            this.pots[0].participants.push(player);
            player.hand = new Hand();
        }
    }

    seatPlayer(player){
        if(this.players.length < this.seats){
            this.players.push(player);
            player.game = this;
        }
    }

    dealPlayers(ncards){
        this.dealer.dealPlayers(ncards,this.participants);
    }


    electWinners(){
        for (let pot of this.pots){
            pot.participants.sort( (a,b) => a.hand.compareTo(b.hand) );
            
            let bestHand = pot.participants[0].hand;            

            pot.winners = pot.participants.filter(p => p.hand.compareTo(bestHand)==0 );
        }
    }
    
    printPlayerHands(){
        this.participants.forEach(player => player.printHand() );
    }

    announceWinners(){
        
        let i=1;
        for(let pot of this.pots){
            pot.winners.sort( (a,b) => a.hand.compareTo(b.hand) );
            console.log("----------------------------------------------------------------");
            console.log("Winner(s) of Pot "+i+" ("+pot.amount+"): ");
            for(let player of pot.winners){
                player.printHand();
            }

            console.log("----------------------------------------------------------------");
            
            i++;
        }
    }

    
}



