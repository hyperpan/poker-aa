let Game = require('./Game');
let Hand = require('../Hands/Hand');

module.exports = class Pot{
    constructor(){
        this.amount = 0;
        this.participants=[];
        this.winners = [];


    }

    addBet(amount){
        this.amount += amount;
    }

    printParticipantHands(){
        for (let participant of this.participants)
            participant.printHand();
    }
}
